# ------ Virtual Cloud Network
resource "oci_core_vcn" "vcn" {
  cidr_block = "10.2.0.0/16"
  compartment_id = var.compartment_ocid
  display_name = "vcn"
}

# ------ Internet Gateway
resource "oci_core_internet_gateway" "ig" {
  compartment_id = var.compartment_ocid
  display_name = "vcn-internet-gateway"
  vcn_id = oci_core_vcn.vcn.id
}

# ------ Route Table for the Public subnet
resource "oci_core_route_table" "public-rt" {
  compartment_id = var.compartment_ocid
  vcn_id = oci_core_vcn.vcn.id
  display_name = "vcn-public-route-table"
  route_rules {
    destination = "0.0.0.0/0"
    network_entity_id = oci_core_internet_gateway.ig.id
  }
  route_rules {
    destination = "10.1.0.0/16"
    network_entity_id = oci_core_drg.drg.id
  }
}

# ------ Security list for the Public Subnet
resource "oci_core_security_list" "public-sl" {
  compartment_id = var.compartment_ocid
  display_name = "vcn-public-subnet-security-list"
  vcn_id = oci_core_vcn.vcn.id
  egress_security_rules {
    protocol = "all"
    destination = "0.0.0.0/0"
  }
  ingress_security_rules {
    protocol = "all"
    source = "0.0.0.0/0"
  }
}

# ------ Public subnet in AD1 in the new VCN
data "oci_identity_availability_domains" "ads" {
  compartment_id = var.compartment_ocid
}
resource "oci_core_subnet" "public-subnet" {
  availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
  cidr_block = "10.2.1.0/24"
  display_name = "vcn-public-subnet"
  compartment_id = var.compartment_ocid
  vcn_id = oci_core_vcn.vcn.id
  route_table_id = oci_core_route_table.public-rt.id
  security_list_ids = [oci_core_security_list.public-sl.id]
  dhcp_options_id = oci_core_vcn.vcn.default_dhcp_options_id
}

# ------ Dynamic Routing Gateway (DRG)
resource "oci_core_drg" "drg" {
  compartment_id = var.compartment_ocid
  display_name = "drg"
}
resource "oci_core_drg_attachment" "test_drg_attachment" {
  drg_id = oci_core_drg.drg.id
  vcn_id = oci_core_vcn.vcn.id
}

# ------ CPE
data "oci_core_cpe_device_shapes" "cpe_shapes" {
}
resource "oci_core_cpe" "cpe" {
  compartment_id = var.compartment_ocid
  ip_address = var.cpe_ip
  cpe_device_shape_id = data.oci_core_cpe_device_shapes.cpe_shapes.cpe_device_shapes.1.cpe_device_shape_id
  display_name = "Azure-CPE"
}

# ------ IPSec connections
resource "oci_core_ipsec" "ipsec-connection" {
  compartment_id = var.compartment_ocid
  cpe_id = oci_core_cpe.cpe.id
  drg_id = oci_core_drg.drg.id
  static_routes = ["10.1.0.0/16"]
  display_name = "ipsec-connection"
}

# ------ Tunnel configurations
data "oci_core_ipsec_connection_tunnels" "ipsec-tunnels" {
  ipsec_id = oci_core_ipsec.ipsec-connection.id
}
resource "oci_core_ipsec_connection_tunnel_management" "tunnel1" {
  ipsec_id = oci_core_ipsec.ipsec-connection.id
  tunnel_id = data.oci_core_ipsec_connection_tunnels.ipsec-tunnels.ip_sec_connection_tunnels.0.id
  display_name = "tunnel1"
  routing = "STATIC"
  ike_version = "V2"
  shared_secret = "123456"
}
