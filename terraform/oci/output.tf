output "vpn-tunnel-endpoint" {
  value = data.oci_core_ipsec_connection_tunnels.ipsec-tunnels.ip_sec_connection_tunnels.0.vpn_ip
}
output "testvm2-ip" {
  value = oci_core_instance.testvm2.public_ip
}
