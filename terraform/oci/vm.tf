# Get Ubuntu Images
data "oci_core_images" "ubuntu-images" {
  compartment_id = var.compartment_ocid
  operating_system = "Canonical Ubuntu"
  operating_system_version = "18.04"
  sort_by = "TIMECREATED"
  sort_order = "DESC"
}

# Create Virtual Machine
resource "oci_core_instance" "testvm2" {
  availability_domain = data.oci_identity_availability_domains.ads.availability_domains.0.name
  compartment_id = var.compartment_ocid
  display_name = "testvm2"
  image = data.oci_core_images.ubuntu-images.images.0.id
  #shape = "VM.Standard.E3.Flex"
  shape = "VM.Standard2.1"
  shape_config {
    ocpus = "1"
  }
  create_vnic_details {
    subnet_id = oci_core_subnet.public-subnet.id
    private_ip = "10.2.1.5"
  }
  metadata = {
    ssh_authorized_keys = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDWDXl/Ojg63OpLdTmhx2VJI8Bc0vnBfO5bG7KXGovbymGJfh2KDDrNByUummXMnxGsdAuDXYOurnmJxtlansc8n36WebTBdgBfoopdbaOZhrWYDOuCEpMhJ7VEex1iiGtNsAn1/cJX/WlVcL8N3z24BNIQBk9r16gBsVrd4VCa2WiyXrF3J+6JDvNd8/Op9Y5UhqWzKaEm8Ylzt5VmcU70Vegte8iRKn0xahBkIznP5lkmZnE8uUnn9rJzzdJKhOw3LuSbflCyr1UOnu18S1uaAcYnlsy7KFcypZrWwJfnSd1VOysmRRMUr96AWaOYgetYIah3j8j4xRT8kmlIuvPb goutaudi@Guillaumes-iMac.local"
  }
}

