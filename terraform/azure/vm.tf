# Create Public IP for Virtual Machine
resource "azurerm_public_ip" "vm-public-ip" {
  name                = "vm-public-ip"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Dynamic"
}

# Create Network Interface for Virtual Machine
resource "azurerm_network_interface" "vm-network-interface" {
  name                = "vm-network-interface"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.azure-subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address = "10.1.1.5"
    public_ip_address_id = azurerm_public_ip.vm-public-ip.id
  }
}

resource "azurerm_virtual_machine" "testvm1" {
  name                  = "testvm1"
  location              = azurerm_resource_group.rg.location
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.vm-network-interface.id]
  vm_size               = "Standard_B1ls"

  delete_os_disk_on_termination = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
  storage_os_disk {
    name              = "myosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "testvm1"
    admin_username = "goutaudi"
  }
  os_profile_linux_config {
    disable_password_authentication = "true"
    ssh_keys {
      key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDWDXl/Ojg63OpLdTmhx2VJI8Bc0vnBfO5bG7KXGovbymGJfh2KDDrNByUummXMnxGsdAuDXYOurnmJxtlansc8n36WebTBdgBfoopdbaOZhrWYDOuCEpMhJ7VEex1iiGtNsAn1/cJX/WlVcL8N3z24BNIQBk9r16gBsVrd4VCa2WiyXrF3J+6JDvNd8/Op9Y5UhqWzKaEm8Ylzt5VmcU70Vegte8iRKn0xahBkIznP5lkmZnE8uUnn9rJzzdJKhOw3LuSbflCyr1UOnu18S1uaAcYnlsy7KFcypZrWwJfnSd1VOysmRRMUr96AWaOYgetYIah3j8j4xRT8kmlIuvPb goutaudi@Guillaumes-iMac.local"
      path = "/home/goutaudi/.ssh/authorized_keys"
    }
  }
}

