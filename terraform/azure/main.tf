# Configure the Azure Provider
provider "azurerm" {
  # whilst the `version` attribute is optional, we recommend pinning to a given version of the Provider
  version = "=2.0.0"
  features {}
}

# Create a resource group
resource "azurerm_resource_group" "rg" {
  name     = "azure-oci-vpn"
  location = "West Europe"
}

# Create a virtual network within the resource group
resource "azurerm_virtual_network" "vnet" {
  name                = "azure-network"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  address_space       = ["10.1.0.0/16"]
}

# Create a subnet within the virtual network
resource "azurerm_subnet" "azure-subnet" {
  name                 = "azure-subnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefix     = "10.1.1.0/24"
}

# Create a GatewaySubnet within the virtual network
resource "azurerm_subnet" "gateway-subnet" {
  name                 = "GatewaySubnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefix     = "10.1.0.0/24"
}

# Create dynamic public IP to be used by the VPN Gateway
resource "azurerm_public_ip" "vpn-gateway-ip" {
  name                = "vpn-gateway-ip"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  allocation_method = "Dynamic"
}

# Create VPN Gateway
resource "azurerm_virtual_network_gateway" "vpn-gateway" {
  name                = "vpn-gateway"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  type     = "Vpn"
  vpn_type = "RouteBased"

  active_active = false
  enable_bgp    = false
  sku           = "Basic"

  ip_configuration {
    name                          = "vpn-gateway-config"
    public_ip_address_id          = azurerm_public_ip.vpn-gateway-ip.id
    private_ip_address_allocation = "Dynamic"
    subnet_id                     = azurerm_subnet.gateway-subnet.id
  }
}

# Create Local Network Gateway
resource "azurerm_local_network_gateway" "oci-vpn-gw" {
  name                = "oci-vpn-gw"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  gateway_address     = var.oci-vpn-gw-ip
  address_space       = ["10.2.0.0/16"]
}

# Create VPN connection
resource "azurerm_virtual_network_gateway_connection" "oci-vpn-connection" {
  name                = "oci-vpn-connection"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  type                       = "IPsec"
  virtual_network_gateway_id = azurerm_virtual_network_gateway.vpn-gateway.id
  local_network_gateway_id   = azurerm_local_network_gateway.oci-vpn-gw.id
  shared_key = "123456"
}

# Create route table and associate with subnet
resource "azurerm_route_table" "route-table" {
  name                          = "route-table"
  location                      = azurerm_resource_group.rg.location
  resource_group_name           = azurerm_resource_group.rg.name
  disable_bgp_route_propagation = true
  route {
    name           = "route1"
    address_prefix = "10.2.0.0/16"
    next_hop_type  = "VirtualNetworkGateway"
  }
}
resource "azurerm_subnet_route_table_association" "route-table-association" {
  subnet_id      = azurerm_subnet.azure-subnet.id
  route_table_id = azurerm_route_table.route-table.id
}


