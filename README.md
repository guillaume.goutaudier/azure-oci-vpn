# Introduction
The objective of this tutorial is to demonstrate how one can easily build a site-to-site IPSec VPN between Microsoft Azure and the Oracle Cloud:
![Azure-OCI-VPN.png](pics/Azure-OCI-VPN.png)

All the components will be deployed using terraform. To save time, we will deploy a unique VPN tunnel, but it is possible to deploy multiple tunnels for redoundancy.

# Pre-requisites
- An Azure account and the `az` command line utility locally installed
- An Oracle Cloud account and the `oci` command line utility locally installed
- Terraform locally installed

# Setup of the Terraform providers
### Azure
- Login to Azure and initialize the Azure provider:
```
cd terraform/azure
az login
terraform init
```

### Oracle Cloud
- Follow the instructions from this guide to setup the right parameters for the Oracle Cloud provider: 
https://community.oracle.com/docs/DOC-1019936
-  Initialize the Oracle Cloud provider:
```
cd terraform/oci
terraform init
```


# Deployment strategy
We will deploy ressources in this order:
- The Azure network and VPN gateway
- The OCI network, Dynamic Routing Gateway (VPN gateway) and IPSec tunnels
- The Azure Local Network Gateway using the IP addresses assigned to the OCI IPSec tunnels
- The Azure connections
- The virtual machines to test the VPN

# Customization to your environment
- Replace the SSH public Key with your own in `terraform/azure/vm.tf` and `terraform/oci/vm.tf`
- Edit `terraform/azure/vm.tf` and update the location variable in the resource group resource with the Azure region you want to use 
- Edit `terraform/oci/terraform.tfvars` and update the region variagle with the Oracle Cloud region you want to use

# Deployment execution
- Deploy all network resources except the VPN connection (this step can take up to 50min):
```
cd terraform/azure
terraform apply -target=azurerm_virtual_network_gateway.vpn-gateway
```
Note that you may need to re-execute this command if the vpn-gateway-ip is not displayed immediately (it means the public IP address is stil being allocated to the VPN gateway). 

- Edit `terraform/oci/terraform.tfvars` and replace the value of `cpe_ip` by the IP address of the Virtual Netowrk Gateway that has just been created

- Create the OCI network resources (this step can take up to 20min):
```
cd terraform/oci
terraform apply -target=oci_core_ipsec.ipsec-connection
terraform apply
```

- Write down the IP of the OCI tunnel endpoints and update `terraform/azure/terraform.tfvars` accordingly

- Complete the Azure deployment with VPN connections and VirtualMachine:
```
cd terraform/azure
terraform apply
```
Note that you may need to re-execute this command if the testvm1-ip is not displayed immediately (it means the public IP address is stil being allocated to the VPN gateway).

# Testing
- Connect to testvm1 (on Azure) and testvm2 (on Oracle Cloud)
``` 
ssh <testvm1-ip>
ssh ubuntu@<testvm2-ip>
```
- Validate that you can ping and ssh to the remote VM using its private IP


# Clean-up
```
cd terraform/azure
terraform destroy
cd terraform/oci
terraform destroy
```


